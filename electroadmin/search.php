<html>
	<head>
		<title>Search Page</title>
	<meta http-equiv="content-type" content="text/html;charset=ISO-8859-1">
	<meta name="generator" content="Lasso Studio for GoLive">
		<style type="text/css" media="screen"><!--
.class { color: #fff; font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
.link { color: #fff; font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-decoration: underline; }
--></style>
	</head>
	<body bgcolor="black">
<form class="class" action="listing.php" method="POST">
			<div class="class">
				<table class="class" border="0" cellspacing="2" cellpadding="2">
					<tr>
						<td colspan="2"><font size="+1"><b>Search for a Hire Sign</b></font>
							<p>To amend, delete or upload an image for a Hire Sign, you first need to find it. Use the search facility below or click 'Find All Records'</p>
						</td>
					</tr>
					<tr>
						<td><strong>Reference Number</strong></td>
						<td><input type="text" name="id" value=""/></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" name="search" value="Search Records"/>
	                                    <input type="submit" name="find_all" value="Find All Records"/>
	                    </td>
					</tr>
				</table>
				<ul>
					<li><a href="index.html"><span class="link">Admin Centre home page</span></a>
					<li><a href="add.php"><span class="link">Add a new sign</span></a>
					<li><a href="search.php"><span class="link">Amend/Delete a current sign</span></a>
					<li><a href="/"><span class="link">Go to the Electro Signs home page</span></a>
				</ul>
			</div>
		</form>
	</body>
</html>
