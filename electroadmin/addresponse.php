<?php
    include_once('../db_settings.php');
    $message = 'You have successfully added a Hire Sign record.';
    if (isset($_REQUEST['submit']) && isset($_REQUEST['id'])) {
        $link = new mysqli($dbHost,$dbUser,$dbPassword,$dbDatabase);
        $query = 'INSERT INTO '.$dbTable.' (`id`, `cat`, `desc`) VALUES ('.$link->real_escape_string($_REQUEST['id']).',"'.$link->real_escape_string($_REQUEST['cat']).'","'.$link->real_escape_string($_REQUEST['desc']).'")';
        $items = $link->query($query) OR ($message = 'An error has occurred.');
    } else {
        $message = 'An error has occurred.';
    }
?>
<html>
	<head>
		<title>Update Page</title>
	<meta http-equiv="content-type" content="text/html;charset=ISO-8859-1">
	<meta name="generator" content="Lasso Studio for GoLive">
		<style type="text/css" media="screen"><!--
.class { color: #fff; font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
.link { color: #fff; font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-decoration: underline; }
--></style>
	</head>
	<body bgcolor="black">

<form class="class" action="updateresponse.php" method="POST">
			<div class="class">
				<table class="class" border="0" cellspacing="2" cellpadding="2">
					<tr>
						<td colspan="2"><font size="+1"><b><?php echo $message; ?></b></font>
					</tr>
				</table>
				<div class="class">
					<ul>
						<li><a href="index.html"><span class="link">Admin Centre home page</span></a>
						<li><a href="add.php"><span class="link">Add a new sign</span></a>
						<li><a href="search.php"><span class="link">Amend/Delete a current sign</span></a>
						<li><a href="/"><span class="link">Go to the Electro Signs home page</span></a>
					</ul>
				</div>
			</div>
		</form>
	</body>
</html>
