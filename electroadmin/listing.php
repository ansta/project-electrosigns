<?php
    if (!isset($_REQUEST['p'])) { $_REQUEST['p'] = 0; } //just because!
    include_once('../db_settings.php');
    $resultsPerpage = 100;
    $currentPage = 0;
    if (isset($_REQUEST['p']) && (int)$_REQUEST['p'] > 0) {
        $currentPage = (int)$_REQUEST['p'];
    }

    $link = new mysqli($dbHost,$dbUser,$dbPassword,$dbDatabase);
    $query = 'SELECT count(*) AS count FROM '.$dbTable;
    if (isset($_REQUEST['id']) && (int)$_REQUEST['id'] > 0) {
        $query .= ' WHERE id LIKE "'.$link->real_escape_string($_REQUEST['id']).'%"';
    }
    $total = $link->query($query)->fetch_object()->count;
    $pagesTotal = intval($total / $resultsPerpage); //that runs because we need to know total numbers before pagination
    //and now the main query with pagination
    $query = 'SELECT * FROM '.$dbTable;
    if (isset($_REQUEST['id']) && (int)$_REQUEST['id'] > 0) {
        $query .= ' WHERE id LIKE "'.$link->real_escape_string($_REQUEST['id']).'%"';
    }
    $query .= ' ORDER BY id';
    if ($currentPage <= $pagesTotal) {
        $startRecord = $currentPage * $resultsPerpage;
    } else {
        $startRecord = 0;
        $resultsPerpage = 0;
    }
    $query .= ' LIMIT '.$startRecord.','.$resultsPerpage;
    //echo $query;
    $items = $link->query($query);
    $actual = $items->num_rows;
    $message = '';
    if (!$items) { $message = $link->error; }
    if ($actual === 0) { $message = 'No records found.'; }
?>
<html>
	<head>
		<title>Results Page</title>
    	<meta http-equiv="content-type" content="text/html;charset=ISO-8859-1">
	    <meta name="generator" content="Lasso Studio for GoLive">
		<link href="../css/basic.css" rel="stylesheet" type="text/css" media="all">
		<style type="text/css" media="screen"><!--
body { color: white; font-size: 12px; font-family: Arial, SunSans-Regular, sans-serif; }
.class { color: #fff; font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
--></style>
	</head>
	<body class="class" bgcolor="black">
		<div class="class">
			<table class="class" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td colspan="4"><font size="+1"><b>Search Results</b></font>
				</tr>
				<tr>
					<td colspan="4"><?php echo $message; ?> To amend or delete a hire sign please click on the reference number below.</td>
				</tr>
				<tr>
					<td><strong>Reference Number</strong></td>
					<td>Uploads</td>
					<td>Uploads</td>
				</tr>
                <?php
                if ($items->num_rows > 0) {
                    while ($item = $items->fetch_object()) {
                        echo '<tr>'.PHP_EOL;
                        echo '<td><a href = "update.php?id='.$item->id.'"> '.$item->id.'</a></td>'.PHP_EOL;
                        echo '<td><a href = "xfileupload.php?jpg='.$item->id.'s" > Click to upload thumbnail image </a></td>'.PHP_EOL;
                        echo '<td><a href = "xfileupload.php?jpg='.$item->id.'b" > Click to upload large image </a></td>'.PHP_EOL;
                        echo '</tr>'.PHP_EOL;
                    }
                }
                ?>
				<tr>
					<td colspan="4">
                    <?php
                        if ($currentPage > 0 && $pagesTotal > 0) {
                            echo '<a href="/electroadmin/listing.php?p=0'.(isset($_REQUEST['id']) && (int)($_REQUEST['id']>0)?'&id='.$_REQUEST['id']:'').'">&lt; &lt; First</a>'.PHP_EOL;
                            echo '<a href="/electroadmin/listing.php?p='.((int)$_REQUEST['p']-1).(isset($_REQUEST['id']) && (int)($_REQUEST['id']>0)?'&id='.$_REQUEST['id']:'').'">&lt; Prev</a>'.PHP_EOL;
                        }
                        if ($actual == 0) {
                            echo 'No records found.';
                        } else {
                            echo 'Showing '.$actual.' records out of '.$total.' found ('.((int)$startRecord + 1).' to '.((int)$startRecord + (int)$actual).')'.PHP_EOL;
                        }
                        if ((int)$_REQUEST['p'] < $pagesTotal && $pagesTotal > 0 ) {
                            echo '<a href="/electroadmin/listing.php?p='.((int)$_REQUEST['p']+1).(isset($_REQUEST['id']) && (int)($_REQUEST['id']>0)?'&id='.$_REQUEST['id']:'').'">Next &gt;</a>'.PHP_EOL;
                            echo '<a href="/electroadmin/listing.php?p='.$pagesTotal.(isset($_REQUEST['id']) && (int)($_REQUEST['id']>0)?'&id='.$_REQUEST['id']:'').'">Last &gt; &gt;</a>'.PHP_EOL;
                        }
                    ?>
						<ul>
							<li><a href="index.html">Admin Centre home page</a>
							<li><a href="add.php">Add a new sign</a>
							<li><a href="search.php">Amend/Delete a current sign</a>
							<li><a href="/">Go to the Electro Signs home page</a>
						</ul>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
