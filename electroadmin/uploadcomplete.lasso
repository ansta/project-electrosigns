<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
		<meta name="generator" content="Adobe GoLive" />
		<title>Electro Signs Photo Upload</title>
		<link href="../css/basic.css" rel="stylesheet" type="text/css" media="all" />
	</head>

	<body class="text" bgcolor="black">
		<p><span class="text"><strong><font size="4">File Upload is Complete</font></strong></span></p>
		<div class="text">
			<div align="left">
				<div align="left">
					<p class="text">[if: action_param:'process' == 'yes'][if: file_uploads->size == 0][else] [get_uploads] [iterate: file_uploads, (local:'item')] [var:'fn'=#item->(find:'upload.realname')] [$fn->(Replace: '.JPG', '.jpg')] [file_copy: #item->(find:'upload.name'), '/uploads/'(var:'fn'), -FileOverWrite] [$fn] [/iterate][/get_uploads][/if][/if]</p>
					<p>[Variable:'fn'] has been successfully uploaded.</p>
					<div class="class">
						<div class="class">
							<div class="class">
								<ul>
									<li><a href="index.lasso"><span class="link">Admin Centre home page</span></a></li>
									<li><a href="add.lasso"><span class="link">Add a new sign</span></a></li>
									<li><a href="search.lasso"><span class="link">Amend/Delete a current sign</span></a></li>
									<li><a href="http://www.electrosigns.co.uk"><span class="link">Go to the Electro Signs home page</span></a></li>
									<li><a href="upload.lasso"><span class="link">Upload an image</span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>

</html>