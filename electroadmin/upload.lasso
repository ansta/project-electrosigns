<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
		<meta name="generator" content="Adobe GoLive" />
		<title>Electro Signs Photo Upload</title>
		<link href="../css/basic.css" rel="stylesheet" type="text/css" media="all" />
	</head>

	<body class="text" bgcolor="black">
		<p><span class="text"><strong><font size="3">Electro Signs Photo Upload</font></strong></span></p>
		<div class="text">
			<p class="text"><strong><font size="2"><span class="text">To upload a file please browse for the file and click 'upload'</span></font></strong></p>
			<p><ls_box marker="1164980834313.12">When a new Hire Sign has been entered into the database you </ls_box><ls_box marker="1164980834313.12">were requested to enter a</ls_box><ls_box marker="1164980834313.12"> reference nu</ls_box><ls_box marker="1164980834313.12">mber. </ls_box><ls_box marker="1164980834313.12">The </ls_box><ls_box marker="1164980834313.12">image </ls_box><ls_box marker="1164980834313.12">name</ls_box><ls_box marker="1164980834313.12"> is taken from the Hire Sign reference number</ls_box><ls_box marker="1164980834313.12">. For example Hire Sign Reference Number</ls_box><ls_box marker="1164980834313.12">: </ls_box><ls_box marker="1164980834313.12">2007 will use 2 images</ls_box><ls_box marker="1164980834313.12">, 1 small, 1 large named 2007s.jpg and 2007b.jpg where </ls_box><ls_box marker="1164980834313.12">'s' is </ls_box><ls_box marker="1164980834313.12">for the small image and 'b' is for the large image. </ls_box></p>
			<p><strong><ls_box marker="1164980834313.12">Image </ls_box><ls_box marker="1164980834313.12">Sizes</ls_box></strong><ls_box marker="1164980834313.12"><br />
					Small/Thumbnail images (</ls_box><ls_box marker="1164980834313.12"><em>&quot;Ref Number&quot;</em></ls_box><ls_box marker="1164980834313.12">s.jpg) </ls_box><ls_box marker="1164980834313.12">need to be 75x75 pixels<br />
				</ls_box><ls_box marker="1164980834313.12">Big images (&quot;<em>Ref Number</em>&quot;</ls_box><ls_box marker="1164980834313.12">b.jpg) need to be 586x488 pixels.</ls_box></p>
			<p><strong><font color="yellow"><ls_box marker="1164980834313.12">ONLY .</ls_box><ls_box marker="1164980834313.12">jpg IMAGES CAN BE USED</ls_box></font></strong></p>
			<p><ls_box marker="1164980834313.12">Any images </ls_box><ls_box marker="1164980834313.12">with th</ls_box><ls_box marker="1164980834313.12">e same reference as images already uploaded will be overwritten with the new version.</ls_box></p>
			<form class="text" action="uploadcomplete.lasso" method="post" enctype="multipart/form-data">
				<p class="text"><input class="text" type="hidden" name="process" value="yes" /> Select a file: <input type="file" name="upload" value="" /><br />
					<input type="submit" value="Upload File" /></p>
			</form>
			<div class="class">
				<div class="class">
					<div class="class">
						<ul>
							<li><a href="index.lasso"><span class="link">Admin Centre home page</span></a></li>
							<li><a href="add.lasso"><span class="link">Add a new sign</span></a></li>
							<li><a href="search.lasso"><span class="link">Amend/Delete a current sign</span></a></li>
							<li><a href="http://www.electrosigns.co.uk"><span class="link">Go to the Electro Signs home page</span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</body>

</html>