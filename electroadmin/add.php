<?php
include_once('../db_settings.php');
$nxt = 0;
$link = new mysqli($dbHost,$dbUser,$dbPassword,$dbDatabase);
$query = 'SELECT max(`id`)+1 AS id FROM `hire`';
$items = $link->query($query);
if ($items->num_rows >0) {
    $item = $items->fetch_object();
    $nxt = $item->id;
}
?>
<html>
	<head>
		<title>Add Page</title>
	<meta http-equiv="content-type" content="text/html;charset=ISO-8859-1">
	<meta name="generator" content="Lasso Studio for GoLive">
		<link href="../css/basic.css" rel="stylesheet" type="text/css" media="all">
		<style type="text/css" media="screen"><!--
.class { color: #fff; font-size: 12px; font-family: Arial, Helvetica, SunSans-Regular, sans-serif; }
--></style>
	</head>
	<body class="class" bgcolor="black">
		<div class="class">
			<form class="class" action="addresponse.php" method="POST">
				<table class="class" border="0" cellspacing="2" cellpadding="2">
					<tr>
						<td colspan="2"><font size="+1"><b>Add a new Hire Sign</b></font>
							<p>To add a new Hire Sign simply enter the details below. The image name is taken from the Hire Sign reference number that is entered below. For example Hire Sign Reference Number: 2007 will use 2 images, 1 small, 1 large named 2007s.jpg and 2007b.jpg where 's' is for the small image and 'b' is for the large image. Image sizes can be found on the upload page.</p>
						</td>
					</tr>
					<tr>
						<td><strong>Reference Number</strong></td>
						<td><input type="Text" name="id" value="<?php echo $nxt; ?>"></td>
					</tr>
					<tr>
						<td><strong>Description</strong></td>
						<td><input type="text" name="desc" value="" size="69"/></td>
					</tr>
					<tr>
						<td><strong>Category</strong></td>
						<td><select name="cat" size="1">
								<option value="adult">Adult</option>
								<option value="drink">Drink/Food</option>
								<option value="lightboxes">Lightboxes</option>
								<option value="logos">Logos</option>
								<option value="oriental">Oriental</option>
								<option value="seasonal">Seasonal</option>
								<option value="shop">Shop</option>
								<option value="other">Other</option>
							</select></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" name="submit" value="Add Record"/>
	                    </td>
					</tr>
				</table>
				<ul>
					<li><a href="index.html">Admin Centre home page</a>
					<li><a href="add.php">Add a new sign</a>
					<li><a href="search.php">Amend/Delete a current sign</a>
					<li><a href="/">Go to the Electro Signs home page</a>
				</ul>
			</form>
		</div>
	</body>
</html>
