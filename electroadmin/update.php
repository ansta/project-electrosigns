<?php
include_once('../db_settings.php');
$message = '';
$link = new mysqli($dbHost,$dbUser,$dbPassword,$dbDatabase);
$query = 'SELECT * FROM '.$dbTable.' WHERE id = '.$link->real_escape_string($_REQUEST['id']).' LIMIT 1';
$items = $link->query($query);
if ($items->num_rows >0) {
    $item = $items->fetch_object();
} else {
    $message = 'An error has occured. ';
}
?>
<html>
	<head>
		<title>Update Page</title>
	<meta http-equiv="content-type" content="text/html;charset=ISO-8859-1">
	<meta name="generator" content="Lasso Studio for GoLive">
		<style type="text/css" media="screen"><!--
.class { color: #fff; font-size: 12px; font-family: Arial, Helvetica, sans-serif; }
.link { color: #fff; font-size: 12px; font-family: Arial, Helvetica, sans-serif; text-decoration: underline; }
--></style>
	</head>
	<body bgcolor="black">
<form class="class" action="updateresponse.php" method="POST">
			<div class="class">
				<table class="class" border="0" cellspacing="2" cellpadding="2">
					<tr>
						<td colspan="2"><font size="+1"><b>Update or Delete a Hire Sign</b></font>
                        <input type="hidden" name="id" value="<?php echo $item->id; ?>"/></td>
					</tr>
					<tr>
						<td colspan="2"><?php echo $message; ?>To amend the Hire Sign simply change the details below and click update record. Or you may delete the Hire Sign from the website by clicking the delete record button below.</td>
					</tr>
					<tr>
						<td><strong>Reference Number</strong></td>
						<td><input type="text" name="id" value="<?php echo $item->id; ?>"/></td>
					</tr>
					<tr>
						<td><strong>Description</strong></td>
						<td><input type="text" name="desc" value="<?php echo $item->desc; ?>" size="69"/></td>
					</tr>
					<tr>
						<td><strong>Category</strong></td>
						<td><select name="cat" size="1">
								<option value="adult" <?php echo $item->cat=='adult'?'selected':''; ?>>Adult</option>
								<option value="drink" <?php echo $item->cat=='drink'?'selected':''; ?>>Drink/Food</option>
								<option value="lightboxes" <?php echo $item->cat=='lightboxes'?'selected':''; ?>>Lightboxes</option>
								<option value="logos" <?php echo $item->cat=='logos'?'selected':''; ?>>Logos</option>
								<option value="oriental" <?php echo $item->cat=='oriental'?'selected':''; ?>>Oriental</option>
								<option value="seasonal" <?php echo $item->cat=='seasonal'?'selected':''; ?>>Seasonal</option>
								<option value="shop" <?php echo $item->cat=='shop'?'selected':''; ?>>Shop</option>
								<option value="other" <?php echo $item->cat=='other'?'selected':''; ?>>Other</option>
							</select></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" name="update" value="Update Record"/>
	                                    <input type="submit" name="delete" value="Delete Record"/>
	                    </td>
					</tr>
				</table>
				<div class="class">
					<ul>
						<li><a href="index.html"><span class="link">Admin Centre home page</span></a>
						<li><a href="add.php"><span class="link">Add a new sign</span></a>
						<li><a href="search.php"><span class="link">Amend/Delete a current sign</span></a>
						<li><a href="/"><span class="link">Go to the Electro Signs home page</span></a>
					</ul>
				</div>
			</div>
		</form>
	</body>
</html>
