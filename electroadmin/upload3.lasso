<?LassoScript
	
	// Tip of the Week
	// Lasso Upload Progress
	// <http://www.omnipilot.com/TotW.1768.9061.lasso>
	// Copyright 2006 OmniPilot Software, Inc.
	//
	// This file demonstrates how a progress graphic can be shown when
	// a file upload starts by toggling the visibility of a div.  
	// Once the file has been uploaded the response file will be shown.
	//
	
	var: 'uploads' = file_uploads;	
	if: $uploads->size > 0;
		// Process uploaded files
	/if;
?>
<html>
	<head>
		<title>Lasso Upload Progress</title>
		<script type="text/javascript">
			function toggleDisplayBlock(idOf)
			{
				var element = document.getElementById(idOf);
				if (element != undefined)
				{
					if (element.style.display == 'none')
					{ element.style.display = 'block'; }
					else
					{ element.style.display = 'none'; }
				}
			}
			function upload_progress()
			{
				toggleDisplayBlock('upload_message');
				return true;
			}
		</script>
		<style>
			div#upload_message {border: solid #999999 1px; background: #eeeeee; width: 100%; font-family: Verdana; font-size: 10px; color: #333333; text-align: center;}
			div#upload_form {align: center; border: solid #999999 1px; background: #eeeeee; width: 288px; font-family: Verdana; font-size: 10px; color: #333333; text-align: center;}
			b.b1,b.b2,b.b3,b.b4 {height:1px; font-size:1px; overflow:hidden; display:block; border-left: solid #999999; border-right: solid #999999;}
			b.b1 {border-width: 0px 5px;}
			b.b2 {border-width: 0px 3px;}
			b.b3 {border-width: 0px 2px;}
			b.b4 {border-width: 0px 1px;}
			div.b5 {margin: 0px 6px;}
			h1 {font-weight: bold; font-size: 12px; text-align: center; background: #999999; margin: 0px; padding: 3px; color: #ffffff;}
			a {font-weight: bold; font-size: 12px; text-align: center; text-decoration: none; color: #666666;}
			a:hover {font-weight: bold; font-size: 12px; text-align: center; text-decoration: none; color: #0000ff;}
			h1 a {font-weight: bold; font-size: 12px; text-align: center; text-decoration: none; color: #ffffff;}
		</style>
	</head>
	<body>
		<div id="upload_form">
			<h1><a href="http://www.omnipilot.com/totw/">Lasso Upload Progress</a></h1>
			<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b><div class="b5">
			<form response="[response_filepath]" enctype="multipart/form-data" method="post" onsubmit="return upload_progress();">
				<br />
				<input type="file" name="upload" />
				<br />
				<br />
				<input type="submit" name="action" value="Upload File" />
			</form>
			<div id="upload_message" style="display: none;">
				Please Wait While
				<br />
				<img src="../../../../../Golf%20Clothing%20Direct/Golf%20Clothing%20Direct/web-content/images/upload.gif" width="144" height="18" border="0" alt="Please Wait" />
				<br />
				Your File Uploads
			</div>
			</div><b class="b4"></b><b class="b3"></b><b class="b2"></b><b class="b1"></b>
		</div>
[if: $uploads->size > 0]
		<hr width="288" align="left" />
		<div id="upload_form">
			<p>The following files were uploaded:</p>
			[iterate: $uploads, (var: 'upload')]
				<p>[encode_html: $upload->(find: 'origname')] ([encode_html: $upload->(find: 'size')] bytes)</p>
			[/iterate]
		</div>
[/if]
	</body>
</html>