<?php echo '<?xml version="1.0" encoding="iso-8859-1"?>';
$message = '';
if (isset($_REQUEST['submit']) && $_REQUEST['jpg'] != '') {
    $target_dir = "../uploads/";
    $newFileName = $_POST['jpg'].'.jpg';
    $target_file = $target_dir.$newFileName;
    $imageFileType = pathinfo($target_dir.basename($_FILES["fileToUpload"]["name"]), PATHINFO_EXTENSION);
    $uploadOk = 1;
// Check if image file is an actual image or fake image
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        if ($check !== false) {
            $message = "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            $message = "File is not an image.";
            $uploadOk = 0;
        }
    }
// Check file size
    if ($_FILES["fileToUpload"]["size"] > 1500000) {
        $message = "Sorry, your file is too large.";
        $uploadOk = 0;
    }
// Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "jpeg") {
        $message = "Sorry, only JPG or JPEG files are allowed.";
        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {

// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) { //if destination file already exists it will be overwritten
            $message = "The file " . basename($_FILES["fileToUpload"]["name"]) . " has been uploaded and renamed to ".$newFileName.".";
        } else {
            $message = "Sorry, there was an error uploading your file.";
        }
    }
} elseif (isset($_GET['jpg']) && $_GET['jpg'] != '') {
    $newFileName = $_GET['jpg'];
} else {
    die('A general error has occurred.');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
		<meta name="generator" content="Adobe GoLive" />
		<title>Upload an image</title>
		<link href="../css/basic.css" rel="stylesheet" type="text/css" media="all" />
	</head>

	<body bgcolor="black">
		<p class="text"><span class="text"><strong><font size="3">Electro Signs Photo Upload</font></strong></span></p>
		<div align="left">
			<p class="text"><span class="text"><span class="std"><font color="white"></font></span></span></p>
			<form action="xfileupload.php" method="post" enctype="multipart/form-data">
				<table class="black" width="721" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td class="text" width="180"><strong>Load an image file </strong></td>
						<td class="text" width="529">You are about to upload an image that is going to be named <strong><?php echo $newFileName; ?></strong></td>
					</tr>
					<tr>
						<td class="text" rowspan="4" width="180">
							<div id="upload_message" style="display: none;">

						</td>
						<td class="text" width="529">
							<div class="text">
								<p><strong>Image Sizes</strong><br/>
										Small/Thumbnail images (<em>&quot;Ref Number&quot;</em>s.jpg) need to be 75x75 pixels<br/>
									    Big images (&quot;<em>Ref Number</em>&quot;b.jpg) need to be 586x488 pixels.</p>
								<p><strong><font color="yellow">ONLY .jpg IMAGES CAN BE USED</font></strong></p>
								<p>Any images with the same reference as images already uploaded will be overwritten with the new version.</p>
							</div>
						</td>
					</tr>
					<tr>
						<td class="text" width="529">
                            <input type="hidden" name="jpg" value="<?php echo $newFileName; ?>">
                            <input type="file" name="fileToUpload" id="fileToUpload">
                        </td>
					</tr>
					<tr>
						<td class="text" width="529"><input type="submit" name="submit" value="Upload" /></td>
					</tr>
					<tr>
						<td class="text" width="529"><strong>Image files may take some time to load up if they are large.</strong></td>
					</tr>
				</table>
			</form>
			<p class="text"><span class="text"><span class="std"><font color="white"></font></span></span></p>
			<p class="text"><font color="white"><span class="text"><span class="std"></span></span></font></p>
			<p class="text"><span class="text"><font color="white"><strong><span class="std"><?php echo $message; ?></span></strong><span class="std"></span></font></span></p>
			<div class="class">
				<div class="class">
					<ul>
						<li><a href="index.html"><span class="link">Admin Centre home page</span></a></li>
						<li><a href="add.php"><span class="link">Add a new sign</span></a></li>
						<li><a href="search.php"><span class="link">Amend/Delete a current sign</span></a></li>
						<li><a href="/"><span class="link">Go to the Electro Signs home page</span></a></li>
					</ul>
				</div>
			</div>
		</div>
	</body>

</html>
