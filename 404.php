<?php header("HTTP/1.0 404 Not Found"); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Page Not Found</title>
</head>

<body>
<h1>Page not found: Error 404 </h1>
<p>The page you have requested is not found. Please try one of the following:</p>
<ul>
  <li>If a page was recently created here, it may not yet be visible because it is still being developed, please try again later or</li>
  <li><a href="/">Click here</a> to go back to home page or</li>
</ul>
</body>
</html>