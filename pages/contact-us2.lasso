<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1" />
		<meta name="generator" content="Adobe GoLive" />
		<title>Electro Signs - Contact Us</title>
		<link href="../css/basic.css" rel="stylesheet" type="text/css" media="all" />
		<style type="text/css" media="screen"><!--
.class { color: white; font-size: 12px; font-family: Arial, SunSans-Regular, sans-serif; }
.large { color: #fff; font-size: 15px; }
--></style>
		<csscriptdict import="import">
			<script type="text/javascript" src="../GeneratedItems/CSScriptLib.js"></script>
		</csscriptdict>
		<csactiondict>
			<script type="text/javascript"><!--
var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		pre_home_over = newImage('../images/home-over.gif');
		pre_neon_over = newImage('../images/neon-over.gif');
		pre_leds_over = newImage('../images/leds-over.gif');
		pre_bulbsigns_over = newImage('../images/bulbsigns-over.gif');
		pre_electroprops_over = newImage('../images/electroprops-over.gif');
		pre_architectural_over = newImage('../images/architectural-over.gif');
		pre_vinyl_over = newImage('../images/vinyl-over.gif');
		pre_signsystems_over = newImage('../images/signsystems-over.gif');
		pre_awnings_over = newImage('../images/awnings-over.gif');
		pre_coldcathode_over = newImage('../images/coldcathode-over.gif');
		pre_traditional_over = newImage('../images/traditional-over.gif');
		pre_maintenance_over = newImage('../images/maintenance-over.gif');
		pre_fascia_over = newImage('../images/fascia-over.gif');
		pre_gallery_over = newImage('../images/gallery-over.gif');
		pre_contactus__over = newImage('../images/contactus--over.gif');
		pre_signbuyer_over = newImage('../images/signbuyer-over.gif');
		preloadFlag = true;
	}
}

// --></script>
		</csactiondict>
	</head>

	<body onload="preloadImages();" bgcolor="black" leftmargin="0" marginheight="30" marginwidth="0" topmargin="30">
		<div align="center">
			<div style="position:relative;width:770px;height:828px;-adbe-g:m;">
				<div style="position:absolute;top:0px;left:0px;width:259px;height:123px;">
					<img src="../images/sign-manufacturer.gif" alt="Electro Signs LTD" height="123" width="259" border="0" /></div>
				<div style="position:absolute;top:0px;left:259px;width:510px;height:123px;">
					<img src="../images/top.gif" alt="Cold Cathode Signs" height="123" width="510" border="0" /></div>
				<div style="position:absolute;top:123px;left:147px;width:194px;height:97px;">
					<img src="../images/top1.gif" alt="Neon Signs" height="97" width="194" border="0" /></div>
				<div style="position:absolute;top:123px;left:341px;width:233px;height:97px;">
					<img src="../images/top2.gif" alt="LED Signs" height="97" width="233" border="0" /></div>
				<div style="position:absolute;top:123px;left:574px;width:195px;height:97px;">
					<img src="../images/top3.gif" alt="Vinyl Graphics" height="97" width="195" border="0" /></div>
				[Include: "../menu.html"]
				<div style="position:absolute;top:720px;left:0px;width:612px;height:107px;">
					<img src="../images/bottom.gif" alt="Electro Signs, 97 Vallentin Road, Walthamstow, London E17 3JJ" height="107" width="612" border="0" /></div>
				<div style="position:absolute;top:672px;left:612px;width:157px;height:155px;">
					<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" height="155" width="157">
						<param name="movie" value="../images/globe%20final.swf" />
						<param name="quality" value="best" />
						<param name="play" value="true" />
						<embed height="155" pluginspage="http://www.macromedia.com/go/getflashplayer" src="../images/globe%20final.swf" type="application/x-shockwave-flash" width="157" quality="best" play="true"></embed> 
					</object></div>
				<div style="position:absolute;top:232px;left:161px;width:587px;height:440px;-adbe-c:c">
					<div align="left">
					
						Thank you for contacting us, we will get back to you as soon as possible to discuss your enquiry.
						<p>[Email_Send: -To='info@electrosigns.co.uk',  -Subject='Contact Form', -Body=(Include: 'contact.txt', -EncodeNone), -From="info@electrosigns.co.uk", -EncodeNone]</p>
					</div>
				</div>
				<div style="position:absolute;top:515px;left:0px;width:147px;height:205px;">
					<img src="../images/lednavbottom.gif" alt="" height="205" width="147" border="0" /></div>
				<div style="position:absolute;top:462px;left:0px;width:147px;height:50px;">
					<a onmouseover="changeImages('signbuyer','../images/signbuyer-over.gif');return true" onmouseout="changeImages('signbuyer','../images/signbuyer.gif');return true" href="http://www.signbuyer.co.uk" target="_blank"><img id="signbuyer" src="../images/signbuyer.gif" alt="Signbuyer.co.uk" name="signbuyer" height="50" width="147" border="0" /></a></div>
			</div>
				To buy signs online visit <a href="http://www.signbuyer.co.uk" target="_blank">Sign Buyer</a>. View our sister company <a href="http://www.coldcathodelight.com" target="_blank">Cold Cathode Light</a><br />
			Website Developed by <a href="http://www.ansta.co.uk" target="_blank">Ansta Ltd</a></div>
	</body>

</html>

























































































































































